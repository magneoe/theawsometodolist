import React from 'react';
import TodoMainScreen from './../screens/main/TodoMainScreen';
import MainTabNavigator from './MainTabNavigator';
import TaskformScreen from './../screens/forms/TaskformScreen'; 
import CategoryformScreen from './../screens/forms/CategoryformScreen';
import TasklistExpanded from './../screens/TasklistExpanded';
import {StackNavigator} from 'react-navigation';
import {Platform, StatusBar, SafeAreaView, View} from 'react-native'; 
import {INITITAL_ROOT_ROUTE} from './../store/global_constants'; 


class RootNavigationWrapper extends React.Component {
  constructor(props, context){
    super(props, context);
  }

  render(){
    return ( 
      <SafeAreaView style={{flex:1, backgroundColor:'#F7F7F7'}}> 
        <RootNavigation /> 
      </SafeAreaView>
    );
  }
}  
 
const RootNavigation = StackNavigator({
  todoMain: {screen: TodoMainScreen},
  mainTabNavigator: {screen: MainTabNavigator},
  taskForm: {screen: TaskformScreen},
  addCategory: {screen: CategoryformScreen},
  tasklistExpanded: {screen: TasklistExpanded},
},
{
  //mode: 'modal',
  headerMode: 'screen',
  initialRouteName: INITITAL_ROOT_ROUTE,
  cardStyle: {
      paddingTop: Platform.OS === 'ios' ? 0 : StatusBar.currentHeight,    
  },
  navigationOptions: {
    header: null,    
  }
});
console.log('App is running...');  
export default RootNavigationWrapper;
