import React from 'react';
import {Platform, StatusBar, View, TabBarIOS, Text, Dimensions} from 'react-native';
import {TabNavigator} from 'react-navigation';
import TaskformScreen from './../screens/forms/TaskformScreen';
import CategoryformScreen from './../screens/forms/CategoryformScreen';

class MainTabNavigator extends React.Component {
  constructor(props, context){
    super(props, context); 
  }

  render(){
    let androidTabNav = (<MyNavigator navigation={this.props.navigation}/>);
    return ( 
      <View style={{flex:1}}>
        {(androidTabNav)} 
      </View>
    );
  }
}  

const iosTabStyle = {
    backgroundColor: '#05A5D1', 
    height: 50,  
    alignItems: 'center',   
    //marginTop: 25,
    justifyContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 3, 
    borderRadius: 3,  
    borderBottomColor: '#666666',           
}
const iosLabelStyle = {
  fontSize: 16,  
  fontWeight: '600',
  width: Dimensions.get('window').width/2,
  height: 50,
  paddingTop: 18,       
}
const androiLabelStyle = {
  fontSize: 16,  
  fontWeight: '600',
}
const androidTabStyle = {
  backgroundColor: '#05A5D1',  
}

const MyNavigator = TabNavigator({ 
  addTask: {screen: TaskformScreen,
  navigationOptions: {
    tabBarLabel: 'Add task',
  },
},
  addCategory: {screen: CategoryformScreen,
    navigationOptions: {
      tabBarLabel: 'Add category',
    },
  }},
  {
    initialRouteName: 'addTask',
    tabBarPosition: 'top',
    animationEnabled: true,
    tabBarOptions: {  
      activeTintColor: '#ffffff', 
      inactiveTintColor: '#737373',   
      labelStyle: (Platform.OS === 'ios' ? iosLabelStyle : androiLabelStyle),
      style: (Platform.OS === 'ios' ? iosTabStyle : androidTabStyle),
    },
    swipeEnabled: true, 
  }

);  

MainTabNavigator.router = MyNavigator.router;

export default MainTabNavigator;
