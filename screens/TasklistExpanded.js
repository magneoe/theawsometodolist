
import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight, SectionList, Alert, CheckBox, Switch, Platform } from 'react-native';
import { PropTypes } from 'prop-types';
import Taskrow from './main/Taskrow';
import store from './../store/TodoStore';
import { DISPAY_CATEGORIES, LIST_TYPES, LIST_TYPE_TODO_ID, LIST_TYPE_REMINDER_ID, ROUTE_TODO_MAIN, LIST_TYPE_WEEKVIEW_ID } from './../store/global_constants';
import { Icon } from 'react-native-elements';
import { NavigationActions } from 'react-navigation';

const styles = StyleSheet.create({
    container: {
        paddingTop: 0,
        backgroundColor: 'transparent',
        flex: 1,
        justifyContent: 'flex-start',
    },
    button: {
        height: 45, 
        borderColor: '#05A5D1', 
        borderWidth: 2,
        backgroundColor: '#333',
        margin: 5,     
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 6 
      }, 
    buttonConfirm: {
        borderColor: '#05A5D1',
        backgroundColor: '#333',
    },
    buttonDelete: {
        borderColor: '#660000',
        backgroundColor: '#802000',
    },
    buttonText: {
        color: '#FAFAFA',
        fontSize: 20,
        fontWeight: '600'
    },
    sectionHeader: {
        backgroundColor: 'rgba(191, 191, 191, 0.4)',
        marginLeft: 20,
        marginRight: 20,
        marginTop: 15,
        borderWidth: 1,
        borderBottomWidth: 3,
        borderColor: 'rgba(191, 191, 191, 1.0)',
        borderRadius: 3,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sectionHeaderTitle: {
        marginLeft: 15,
        marginBottom: 5,
        fontWeight: '500',
        fontSize: 14,
        alignSelf: 'center', 
    },
    columnCheckbox: {
        flex: 2,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
    },
    columnDelete: {
        flex: 2,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
    },
    titleConstainer: {
        flex: 4,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    iconStyle: {
        marginLeft: 15,
    },
    header: {
        paddingTop: 10,   
        flexDirection: 'row',   
        justifyContent: 'flex-start',
        borderBottomWidth: 1,
        borderBottomColor: '#d9d9d9',  
    }
});

class TasklistExpanded extends React.Component {
    static navigationOptions = ({ navigation }) => ({
        header: (
            <View style={styles.header}>
                <Icon name={'chevron-left'} onPress={() => { navigation.goBack(); }} size={35} />  
                <Text style={styles.sectionHeaderTitle}>{navigation.state.params.headerTitle}</Text>
            </View>
        )
    });

    constructor(props, context) {
        super(props, context);
        this.state = store.getState();
        store.subscribe(() => {
            this.setState(store.getState());
            this.init(props);
        });
        this.init(props);
        this.checkAllActive = this.selectedCategories.map(cat => { return false; });
    }

    init = (props) => {

        const { onRemoveTask, onRemoveAllTasks, onCheckTask, editTask, onAddNewItem,
            onDeleteCategory, selectedCategories, getTotalCategoryLength } = props.navigation.state.params;

        this.onRemoveTask = onRemoveTask;
        this.onRemoveAllTasks = onRemoveAllTasks;
        this.onCheckTask = onCheckTask;
        this.editTask = editTask;
        this.onAddNewItem = onAddNewItem;
        this.onDeleteCategory = onDeleteCategory;
        this.selectedCategories = selectedCategories;
        this.getTotalCategoryLength = getTotalCategoryLength;

        this.sections = this.selectedCategories.map(cat => {
            cat.length = cat.todos.length;
            return { data: cat.todos, title: cat.name, id: cat.id, cat };
        })
    }
    componentWillReceiveProps(nextProps) {
        this.init(nextProps);
    }

    deleteCategory = (selectedCategories, goBack) => {
        let categoriesToDelete = selectedCategories.map(cat => { if (!cat.readOnly) return cat.name });
        let alertText = '';
        if (categoriesToDelete.length > 0)
            alertText = 'Delete category: "' + categoriesToDelete.join(', ') + '"?';
        else
            alertText = 'The selected category/categories are read only';
        Alert.alert(
            'Delete category', alertText,
            [
                { text: 'Ok', onPress: () => this.onDeleteCategory(selectedCategories, goBack) },
                { text: 'Cancel', onPress: () => { } },
            ]
        );
    }
    keyExtractor = (item, index) => { return item.id }

    renderRow = (item) => {
        return (
            <Taskrow listItem={item.item}
                onRemoveTask={this.onRemoveTask}
                onCheckTask={this.onCheckTask}
                editTask={this.editTask}
                category={item.section.cat}
                showHeaderBackButton={true} /> 
        );
    }
    checkAll = (selectedCategory) => {
        if (selectedCategory.todos.length === 0)
            return;
        let indexOfCategory = this.selectedCategories.findIndex(cat => { return cat.id === selectedCategory.id });
        Alert.alert(
            'Check all', 'Mark all tasks as ' + (this.checkAllActive[indexOfCategory] == true ? "done" : "not done") + '?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.onCheckTask(selectedCategory.todos, selectedCategory, this.checkAllActive[indexOfCategory]);
                    }
                },
                { text: 'Cancel', onPress: () => { this.checkAllActive[indexOfCategory] = !this.checkAllActive[indexOfCategory]; this.setState({}); } },
            ],
            { cancelable: false }
        );
    }
    deleteAllTasks = (selectedCategory) => {
        if (selectedCategory.todos.length === 0)
            return;
        Alert.alert(
            'Delete', 'Delete all tasks?',
            [
                {
                    text: 'Ok', onPress: () => {
                        this.onRemoveAllTasks(selectedCategory);
                    }
                },
                { text: 'Cancel', onPress: () => { } },
            ],
        );

    }

    renderSectionHeader = ({ section }) => {
        let listType = section.cat.listType;
        var placeholderIcon = <View />;
        var placeholderLength = (<Text style={styles.sectionHeaderTitle}>{section.title}</Text>);
        let indexOfCategory = this.selectedCategories.findIndex(cat => { return cat.id === section.cat.id });

        if (listType === LIST_TYPE_TODO_ID) {
            placeholderIcon = <Icon type="MaterialIcons" name="low-priority" size={20} iconStyle={styles.iconStyle} />;
            placeholderLength =
                (<Text style={styles.sectionHeaderTitle}>
                    {section.title} ({section.data.length}/{this.getTotalCategoryLength(section.cat)})
                </Text>);
        }
        else if (listType == LIST_TYPE_REMINDER_ID) {
            placeholderIcon = <Icon type="evilicon" name="clock" size={20} iconStyle={styles.iconStyle} />
            placeholderLength =
                (<Text style={styles.sectionHeaderTitle}>
                    {section.title} ({section.data.length}/{this.getTotalCategoryLength(section.cat)})
            </Text>);
        }
        else if (listType === LIST_TYPE_WEEKVIEW_ID) { }

        return (
            <View style={styles.sectionHeader}>
                <View style={[styles.column, styles.titleConstainer]}>
                    <TouchableHighlight underlayColor={'rgba(0,0,0,0.1)'}
                        delayLongPress={1000}
                        onLongPress={() => { this.deleteCategory(this.selectedCategories, true) }} >
                        {placeholderLength}
                    </TouchableHighlight>
                    {placeholderIcon}
                </View>
                <View style={styles.columnCheckbox}>
                    {(Platform.OS === 'ios' ?
                        <Switch
                            onValueChange={(isChecked) => { this.checkAllActive[indexOfCategory] = isChecked; this.checkAll(section.cat); this.setState({}); }}
                            disabled={false}
                            value={this.checkAllActive[indexOfCategory]}
                            style={styles.checkAll} />
                        :
                        <CheckBox
                            onValueChange={(isChecked) => { this.checkAllActive[indexOfCategory] = isChecked; this.checkAll(section.cat); this.setState({}); }}
                            disabled={false}
                            value={this.checkAllActive[indexOfCategory]}
                            style={styles.checkAll} />
                    )}
                </View>
                <View style={styles.columnDelete}>
                    <Icon type="MaterialIcons" name="delete" iconStyle={styles.iconDelete}
                        onPress={() => { this.deleteAllTasks(section.cat); }} />
                </View>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.container}>
                <SectionList
                    keyExtractor={this.keyExtractor}
                    renderItem={this.renderRow}
                    extraData={this.state}
                    renderSectionHeader={this.renderSectionHeader}
                    sections={this.sections} />

                <TouchableHighlight style={[styles.button, styles.buttonConfirm]} onPress={() => { this.onAddNewItem(this.selectedCategories) }} >
                    <Text style={styles.buttonText}>Add</Text>
                </TouchableHighlight>

                {(this.selectedCategories.length === 1 ? ( //Deleting only supports one category for now
                    <TouchableHighlight style={[styles.button, styles.buttonDelete]}
                        onPress={() => { this.deleteCategory(this.selectedCategories, true) }}>
                        <Text style={styles.buttonText}>Delete category</Text>
                    </TouchableHighlight>) : (<View />))}
            </View>
        );
    }
}

export default TasklistExpanded;