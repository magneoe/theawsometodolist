import { Alert, DatePickerAndroid, TimePickerAndroid } from 'react-native';

export function pickTimeAndDate(initialDate, updateDateCallback) {
  new Promise((resolve, reject) => {
    openCalendar(resolve, reject, initialDate);
  }).then((newDate) => {
    new Promise((resolve, reject) => {
      openTimepicker(resolve, reject, initialDate); 
    }).then((newTime) => { 
      let unconfirmedDate = new Date(
        newDate.getFullYear(), 
        newDate.getMonth(), 
        newDate.getDate(),   
        newTime.getHours(), 
        newTime.getMinutes(), 
        0, 0); 
      if (unconfirmedDate.getTime() <= new Date().getTime()) {
        Alert.alert('Error', 'Must be future date!',
          [ { text: 'Ok' }]); 
      }
      else 
        updateDateCallback(unconfirmedDate);//Success  
    }).catch((err) => { console.log('Error:', err)});
  }).catch((err) => {console.log('Error:', err)}); 

}
export function pickDate(initialDate, updateDateCallback) {
  new Promise((resolve, reject) => {
    openCalendar(resolve, reject, initialDate)
  }).
    then((newDate) => {  
      updateDateCallback(newDate); //Success      
    }).catch(() => { console.log('Cancel!') });
}


openCalendar = (resolve, reject, initialDate) => {
  const { action, day, month, year } = DatePickerAndroid.open({
    minDate: new Date(),
    date: initialDate,
  }).then(res => { 
    if (res.action !== DatePickerAndroid.dismissedAction) {
      let newDate = new Date();  
      newDate.setFullYear(res.year, res.month, res.day);
      resolve(newDate);  
    }
    else
      reject();
  }).catch = (error) => {
    console.log('Cannot open date picker', error);
    reject();
  }
}
openTimepicker = (resolve, reject, initialDate) => {
  const { action, hour, minute } = TimePickerAndroid.open({
    hour: initialDate.getHours(),
    minute: initialDate.getMinutes(),     
    is24Hour: true,
  }).then(res => {
    if (res.action !== TimePickerAndroid.dismissedAction) {
      let newDate = new Date();
      newDate.setHours(res.hour);
      newDate.setMinutes(res.minute);
      resolve(newDate); 
    }
    else
      reject(); 
  }).catch = (error) => {
    console.log('Cannot open date picker', error);
    reject();
  }
}