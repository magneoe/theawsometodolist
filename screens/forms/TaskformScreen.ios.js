import React from 'react';
import {
  Text, TextInput, View, TouchableHighlight, Modal, Picker, StyleSheet,
  Alert, DatePickerAndroid, TimePickerAndroid, Switch, KeyboardAvoidingView,
  Dimensions, DatePickerIOS, 
} from 'react-native';
import { PropTypes } from 'prop-types';
import { NavigationActions } from 'react-navigation';
import store from './../../store/TodoStore';
import { ROUTE_TODO_MAIN, PRIORITIES, PRI_LOW_ID, LIST_TYPES, LIST_TYPE_TODO_ID } from './../../store/global_constants';
import { Icon } from 'react-native-elements';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: 5,
    backgroundColor: '#F7F7F7',
    height: Dimensions.get('window').height - 70,
  },
  input: {
    borderWidth: 1,
    borderColor: '#D7D7D7',
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
    borderRadius: 4,
  },
  inputTitle: {
    height: 50,
  },
  buttonText: {
    fontWeight: '600',
    fontSize: 20,
  },
  button: {
    height: 45,
    alignSelf: 'stretch',
    backgroundColor: '#05A5D1',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    borderColor: '#ffffff',
    borderWidth: 1,
  },
  cancelButton: {
    backgroundColor: '#666',

  },
  label: {
    marginLeft: 10,
    fontSize: 15,
    fontWeight: '500',
  },
  flexContainer: {
    flex: 1,
  },
  oneLineContainer: {
    flex: 1,
    height: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  icon: {
    marginRight: 12,
  },
  timeLabel: {
    marginLeft: 20,
    fontSize: 14,
    fontWeight: '100',
  },
  description: {
    height: Dimensions.get('window').height / 8,
  },
  dropDownItemStyle: {
    height: Dimensions.get('window').height / 10,
  },
  datePicker: {
    height: Dimensions.get('window').height / 6,
  },
  description: {
    height: Dimensions.get('window').height / 5,
  },
  header: {
    paddingTop: 10,   
    flexDirection: 'row',   
    justifyContent: 'flex-start',
    borderBottomWidth: 1,
    borderBottomColor: '#d9d9d9',  
}
});

class TaskformScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
    const { title, showHeaderBackButton } = navigation.state.params;
    if (showHeaderBackButton) {
      return {
        header: (
        <View style={styles.header}> 
          <Icon name={'chevron-left'} onPress={() => {
             navigation.goBack(); 
          }} size={35} /> 
          <Text style={styles.sectionHeaderTitle}>{title}</Text>
      </View>)
      }
  }
}

  constructor(props, context) {
    super(props, context);
    this.state = store.getState();
    store.subscribe(() => {
      this.setState(store.getState());
    });
    this.init(props);
  }

  init = (props) => {
    const { params } = props.navigation.state;
    this.selectedCategories = params.selectedCategories; 
    this.showHeaderBackButton = params.showHeaderBackButton;

    //Default values
    this.selectedCategory = {id:''};
    this.task = {timeEnabled:false};
    this.showCalendar = false;
    this.showCalendarTime = false;

    let startDate = new Date();
    startDate.setTime(startDate.getTime() + (1000 * 60 * 60 * 2));

    if (this.selectedCategories !== undefined && 
      this.selectedCategories !== null &&
      this.selectedCategories.length > 0) {
      //Init list type
      this.listType = this.selectedCategories[0].listType;
      this.selectedCategory = this.selectedCategories[0];
      this.formerCategory = this.selectedCategory;

      //To edit task
      if (params.toEdit !== undefined && params.toEdit !== null) {
        this.task = params.toEdit;
        if (!this.task.timeEnabled)
          this.task.date = startDate;
      }
      //Init new task
      else {
        this.initNewTask(params, this.selectedCategory, startDate);
      }

      //Init pickers
      this.categoryItems = this.selectedCategories.map(cat => {
        return <Picker.Item key={cat.id} value={cat.id} label={cat.name} />
      });
    }
    this.priorityItems = PRIORITIES.map(priority => {
      return <Picker.Item key={priority.id} value={priority.id} label={priority.name} />
    });
    this.unconfirmedDate = new Date(this.task.date);
  }

  initNewTask = (params, selectedCategory, startDate) => {
    this.task = {
      title: '',
      description: '',
      isActive: true,
      priority: PRI_LOW_ID,
      outputLabel: '',
      timeEnabled: (selectedCategory.listType === LIST_TYPE_TODO_ID ? false : true),
      date: new Date(startDate.getTime()), //Appear to be an IOS bug here  
      listType: selectedCategory.listType,
    };
  }

  setPriorityTask = () => {
    let outputLabel = '';
    let priority = PRIORITIES.find(pri => { return pri.id === this.task.priority }).name;
    if (this.task.timeEnabled)
      outputLabel += this.getCurrentDate() + '(' + priority + ')';
    else
      outputLabel = priority;
    this.task.outputLabel = outputLabel;
  }
  setTimeboundTask = () => {
    delete this.task.priority;
    if (this.task.timeEnabled) {
      this.task.outputLabel = this.getCurrentDate() + this.getCurrentTime();
    }
    else {
      this.task.outputLabel = '-';
    }
  }

  onCancel = () => {
    const {goBack} = this.props.navigation;
    goBack(null);
  }
  onSubmit = () => {
    //Simple validation of title
    if (this.task.title === undefined || this.task.title.length < 1) {
      Alert.alert('Error', 'A title must be set!',
        [
          { text: 'Ok' },
        ]);
    }
    //Validation of time
    else if (this.task.timeEnabled && this.task.date.getTime() <= new Date().getTime()) {
      Alert.alert('Error', 'Time must be in the future!',
        [
          { text: 'Ok' },
        ]);
    }
    //Success
    else {
      const { goBack } = this.props.navigation;
      const { params } = this.props.navigation.state;

      switch (this.listType) {
        case 1:
          this.setPriorityTask();
          break; 
        case 2:
          this.setTimeboundTask();
          break;
      }
      params.onAddTask(this.task, this.selectedCategory, this.formerCategory);
      goBack(null); 
    } 
  }

  onChangeTitle = (text) => {
    this.task.title = text;
  }
  onChangeDescription = (text) => {
    this.task.description = text;
  }
  onNewPriority = (itemValue, itemIndex) => { this.task.priority = itemValue; this.setState({}); }

  onNewCategory = (itemValue, itemIndex) => {
    this.selectedCategory = this.selectedCategories.find(cat => { return cat.id === itemValue });
    this.listType = this.selectedCategory.listType;
    this.task.listType = this.selectedCategory.listType;
    this.task.timeEnabled = (this.listType === LIST_TYPE_TODO_ID ? false : true);
    this.setState({});
  }

  componentWillReceiveProps = (nextProps) => {
    //Because the dropdown list of categories is dependent on this.selectedCategoryId
    // we need to update it
    this.init(nextProps);
  }
  getCurrentDate = () => {
    let label = '';
    try {
      let utcString = this.task.date.toUTCString().split(' ');
      label += utcString[0] + ' ';
      label += utcString[1] + ' ';
      label += utcString[2] + ' ';
    } catch (error) { }
    return label;
  }
  getCurrentTime = () => {
    let label = '-';
    try {
      label = (this.task.date.getHours() < 10 ? '0' + this.task.date.getHours() : this.task.date.getHours()) + ':' +
        (this.task.date.getMinutes() < 10 ? '0' + this.task.date.getMinutes() : this.task.date.getMinutes());
    }
    catch (error) { }
    return label;
  }
  setTime = (newDate) => {
    this.unconfirmedDate = new Date(newDate.getTime());
    this.setState({});
  }
  submitDateDialog = () => {
    this.task.date = new Date(this.unconfirmedDate.getTime());
    this.cancelDateDialog();
  }
  cancelDateDialog = () => {
    this.showCalendar = false;
    this.showCalendarTime = false;
    this.unconfirmedDate = new Date(this.task.date.getTime());
    this.setState({});
  }
  openCalendar = () => {
    if (this.task.timeEnabled) {
      this.showCalendar = true;
      this.setState({});
    }
  }
  openTimepicker = () => {
    if (this.task.timeEnabled) {
      this.showCalendarTime = true;
      this.setState({});
    }
  }
  enableTime = (isChecked) => {
    this.task.timeEnabled = isChecked;
    if (isChecked && this.task.date <= (new Date().getTime())) {
      let currDate = new Date();
      this.task.date = currDate.setTime(currDate.getTime() + (1000 * 60 * 60 * 2));
    }
    this.setState({});
  }
  showPriorityView = () => {
    let dateRow = (<View style={styles.oneLineContainer}>
      <Text style={styles.timeLabel}> Date: {this.getCurrentDate()} </Text>
      <Icon type="font-awesome" name="calendar" iconStyle={styles.icon}
        onPress={() => { this.openCalendar() }} />

      <Modal visible={this.showCalendar}
        animationType='slide'
        presentationStyle='formSheet'>
        <DatePickerIOS date={this.unconfirmedDate}
          onDateChange={(newDate) => { this.setTime(newDate); }}
          minimumDate={new Date()}
          mode='date' />
        <TouchableHighlight style={styles.button} onPress={() => this.submitDateDialog()}>
          <Text style={styles.buttonText}>Ok</Text>
        </TouchableHighlight>
        <TouchableHighlight style={[styles.button, styles.cancelButton]} onPress={() => this.cancelDateDialog()}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableHighlight>
      </Modal>
    </View>);

    return (
      <View style={styles.flexContainer}>
        <Text style={styles.label}>Pick priority:</Text>
        <Picker style={styles.input}
          mode={'dropdown'}
          selectedValue={this.task.priority}
          onValueChange={this.onNewPriority}
          itemStyle={styles.dropDownItemStyle}>
          {this.priorityItems}
        </Picker>
        <View style={styles.oneLineContainer}>
          <Text style={styles.label}>Enable date:</Text>
          <Switch style={styles.input}
            onValueChange={(isChecked) => { this.enableTime(isChecked); }}
            disabled={false}
            value={this.task.timeEnabled} />
        </View>
        {(this.task.timeEnabled ? dateRow : <View style={styles.flexContainer}></View>)}
      </View>
    );
  }
  showTimebasedView = () => {

    let dateRow = (<View style={styles.oneLineContainer}>
      <Text style={styles.timeLabel}>Date: {this.getCurrentDate()} - Clock: {this.getCurrentTime()}</Text>
      <Icon type="font-awesome" name="calendar" iconStyle={styles.icon}
        onPress={() => { this.openTimepicker(); }} />

      <Modal visible={this.showCalendarTime}
        animationType='slide'
        presentationStyle='formSheet'>
        <DatePickerIOS date={this.unconfirmedDate}
          onDateChange={(newDate) => { this.setTime(newDate); }}
          minimumDate={new Date()}
          mode='datetime'
          minuteInterval={15} />
        <TouchableHighlight style={styles.button} onPress={() => this.submitDateDialog()}>
          <Text style={styles.buttonText}>Ok</Text>
        </TouchableHighlight>
        <TouchableHighlight style={[styles.button, styles.cancelButton]} onPress={() => this.cancelDateDialog()}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableHighlight>
      </Modal>
    </View>);
    return (
      <View style={styles.flexContainer}>
        <View style={styles.oneLineContainer}>
          <Text style={styles.label}>Enable date:</Text>
          <Switch style={styles.input}
            onValueChange={(isChecked) => { this.enableTime(isChecked); }}
            disabled={false}
            value={this.task.timeEnabled} />
        </View>
        {(this.task.timeEnabled ? dateRow : <View style={styles.flexContainer}></View>)}
      </View>
    );
  }
  render() {

    return (
        <KeyboardAvoidingView style={styles.container}
          contentContainerStyle={styles.container}
          resetScrollToCoords={{ x: 0, y: 0 }}
          behavior='position'>
          <Text style={styles.label}>Pick category:</Text>
          <Picker style={styles.input}
            itemStyle={styles.dropDownItemStyle}
            mode={'dropdown'}
            selectedValue={this.selectedCategory.id}
            onValueChange={this.onNewCategory}>
            {this.categoryItems}
          </Picker>

          {(this.listType === LIST_TYPE_TODO_ID ? this.showPriorityView() : this.showTimebasedView())}

          <Text style={styles.label}>Title:</Text>
          <TextInput style={[styles.input, styles.inputTitle]}
            onChangeText={this.onChangeTitle}
            placeholder={"Title"}
            defaultValue={this.task.title}
            autoCapitalize={'sentences'} />

          <Text style={styles.label}>Description:</Text>
          <TextInput style={[styles.input, styles.description]}
            onChangeText={this.onChangeDescription}
            placeholder={"Description"}
            defaultValue={this.task.description}
            multiline={true}
            autogrow={false}
            blurOnSubmit={false}
            autoCapitalize={'sentences'}
          />
          <TouchableHighlight style={styles.button} onPress={this.onSubmit}>
            <Text style={styles.buttonText}>Add/Update</Text>
          </TouchableHighlight>
          {(this.showHeaderBackButton ? <View /> : 
            <TouchableHighlight style={[styles.button, styles.cancelButton]} onPress={this.onCancel}>
              <Text style={styles.buttonText}>Back</Text>
            </TouchableHighlight>)}
        </KeyboardAvoidingView>
    );
  }
}
export default TaskformScreen;
