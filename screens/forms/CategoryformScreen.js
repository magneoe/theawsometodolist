
import React from 'react';
import { Text, View, StyleSheet, TextInput, TouchableHighlight, 
  Keyboard, Alert, Picker, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation';
import { ROUTE_TODO_MAIN, ROUTE_ADD_TASK, LIST_TYPES, LIST_TYPE_TODO_ID } from './../../store/global_constants';
import store from './../../store/TodoStore';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingTop: 30,
    backgroundColor: '#F7F7F7'
  },
  label: {
    marginLeft: 10,
    fontSize: 15,
    fontWeight: '500',
  },
  input: {
    borderWidth: 1,
    borderColor: '#D7D7D7',
    marginLeft: 10,
    marginRight: 10,
    padding: 15,
    borderRadius: 3,
  },  
  button: {
    height: 45,
    alignSelf: 'stretch',
    backgroundColor: '#05A5D1',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6, 
    borderColor: '#ffffff',
    borderWidth: 1,
  },
  cancelButton: {
    backgroundColor: '#666',
  },
  buttonText: {
    fontWeight: '600',
    fontSize: 20,
  },
  dropDownItemStyle: {
    height: Dimensions.get('window').height / 10,
  }
});
class CategoryformScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props, context) {
    super(props, context);

    this.state = store.getState();

    //Adding data to the listType picker
    this.listTypeItems = LIST_TYPES.map(listType => {
      return <Picker.Item key={listType.id} value={listType.id} label={listType.name} />
    });
    //Setting default selection on thelistType picker
    this.selectedListType = LIST_TYPE_TODO_ID;
  }

  onChangeText = (text) => {
    this.categoryName = text;
  }
  onCancel = () => {
    this.categoryName = '';
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: ROUTE_TODO_MAIN }),
      ]
    });
    this.props.navigation.dispatch(resetAction);
  }
  onSubmit = () => {
    Keyboard.dismiss();
    if (this.categoryName.length < 1) { 
      Alert.alert('Error', 'Too short name!',
        [
          { text: 'Ok' },
        ]);
    }
    let { params } = this.props.navigation.state;
    let category = params.addCategory(this.categoryName, this.selectedListType);

    //Valid categoryName
    if (category !== null) {
      params.selectedCategories = [category].concat(this.state.categories); 
      const navigateAction = NavigationActions.navigate({
        routeName: ROUTE_ADD_TASK,
        params, 
      });
      this.props.navigation.dispatch(navigateAction);
      this.categoryName = '';
      this.setState({});   
    }
    else { //Invalid category name
      Alert.alert('Error', 'Name already exists!',
        [
          { text: 'Ok' },
        ]);
    } 
  }

  onNewListType = (itemValue, itemIndex) => { this.selectedListType = itemValue; this.setState({}) }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.label}>Category name:</Text>
        <TextInput
          style={styles.input}
          onChangeText={this.onChangeText}
          placeholder={'Name'}
          value={this.categoryName} 
          autoCapitalize={'sentences'} />  

        <Text style={styles.label}>Pick list type:</Text>
        <Picker
          style={styles.input}
          itemStyle={styles.dropDownItemStyle}
          mode={'dropdown'}
          selectedValue={this.selectedListType}
          onValueChange={this.onNewListType}>
          {this.listTypeItems}
        </Picker>

        <TouchableHighlight style={styles.button} onPress={() => this.onSubmit()}>
          <Text style={styles.buttonText}>Ok</Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[styles.button, styles.cancelButton]}
          onPress={() => this.onCancel()}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
CategoryformScreen.propTypes = {
  addCategory: PropTypes.func.isRequired,
}

export default CategoryformScreen;
