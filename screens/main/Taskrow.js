import React from 'react';
import { Text, View, StyleSheet, TouchableHighlight, CheckBox, Alert, Platform, Switch } from 'react-native';
import { PropTypes } from 'prop-types';
import { LIST_TYPE_TODO_ID, LIST_TYPE_REMINDER_ID } from './../../store/global_constants';
 

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
    borderWidth: 1,
    borderColor: '#d9d9d9',
    borderRadius: 0,
    padding: 5, 
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
  },
  containerInactive: {
    backgroundColor: 'rgba(89, 89, 89,0.2)',
  },
  priorityHIGH: {
    borderRightWidth: 3,
    borderRightColor: '#ff000c',
  },
  priorityNORMAL: {
    borderRightWidth: 3,
    borderRightColor: '#faff00',
  },
  priorityLOW: {
    borderRightColor: '#00a30d',
  },
  title: {
    fontSize: 16,
    fontWeight: '300',
  },
  btnDone: {
    borderRadius: 5,
    borderColor: '#FFFFFF',
    padding: 5,
    backgroundColor: '#EAEAEA',
  },
  columnCheckbox: {
    flex: 2, 
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  columnOuputlabel: { 
    flex: 2, 
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleContainer: {
    flex: 4,     
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
});

deleteItem = (props) => {
  Alert.alert(
    'Delete item', 'Delete item: "' + props.listItem.title + '"?',
    [
      { text: 'Ok', onPress: () => props.onRemoveTask(props.listItem, props.category) },
      { text: 'Cancel', onPress: () => { } },
    ]
  );
}



const Taskrow = (props) => {

  let containerStyles = [styles.container];
  let listType = props.listItem.listType; 

  //Setting styles based on listType
  if (!props.listItem.isActive)
    containerStyles.push(styles.containerInactive);
  switch (listType) {
    case LIST_TYPE_TODO_ID: 
      if(props.listItem.priority === 2)
        containerStyles.push(styles['priorityNORMAL']);
      else if(props.listItem.priority === 3)
        containerStyles.push(styles['priorityHIGH']);
      break;
    case LIST_TYPE_REMINDER_ID: 
      break;
    default:
  }

  let outputLabel = props.listItem.outputLabel;
  return (
    <TouchableHighlight
      onPress={() => props.editTask(props.listItem, props.category, props.showHeaderBackButton)} 
      delayLongPress={500}
      onLongPress={() => { this.deleteItem(props) }}
      activeOpacity={0.5}
      underlayColor={'rgba(0,0,0,0)'}>

      <View style={containerStyles}>
        <View style={[styles.column, styles.titleContainer]}>
          <Text style={styles.title} >{props.listItem.title}</Text>
        </View>
        <View style={styles.columnCheckbox}>
          {( Platform.OS === 'ios' ? 
           <Switch
           onValueChange={(isChecked) => { props.onCheckTask([props.listItem], props.category, isChecked) }}
           disabled={false}
           value={!props.listItem.isActive} /> 
          :
          <CheckBox
            onValueChange={(isChecked) => { props.onCheckTask([props.listItem], props.category, isChecked) }} 
            disabled={false}
            value={!props.listItem.isActive} />
          )}
        </View>
        <View style={styles.columnOuputlabel}>
          <Text>{outputLabel}</Text>
        </View>
      </View>
    </TouchableHighlight>
  );
};

Taskrow.propTypes = { 
  onCheckTask: PropTypes.func.isRequired,
  onRemoveTask: PropTypes.func.isRequired,
  listItem: PropTypes.object.isRequired,
  editTask: PropTypes.func.isRequired,
  category: PropTypes.object.isRequired,
  showHeaderBackButton: PropTypes.bool.isRequired,  
};

export default Taskrow;