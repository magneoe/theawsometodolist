import React from 'react';
import { View, Text, Modal, TouchableHighlight, StyleSheet, Dimensions, ScrollView, Alert } from 'react-native';
import store from './../../store/TodoStore';
import { Icon } from 'react-native-elements';
import { PropTypes } from 'prop-types';
import GestureRecognizer, { swipeDirections } from 'react-native-swipe-gestures';
import Prompt from 'react-native-actually-usable-prompt';
import { MenuProvider } from 'react-native-popup-menu';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import { renderers } from 'react-native-popup-menu';
const { SlideInMenu } = renderers;  

const { width, height } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonText: {
    fontWeight: '600',
    fontSize: 20, 
  },
  button: {
    height: 45,
    alignSelf: 'stretch',
    backgroundColor: '#05A5D1',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 6,
    borderColor: '#ffffff',
    borderWidth: 1,
  },
  cancelButton: {
    backgroundColor: '#666',     
  },
  icon: {
    marginRight: 12,
  },
  actionMenu: {
    backgroundColor: '#05A5D1',
    alignSelf: 'center', 
    marginTop: 50,  
    fontWeight: '600',
    fontSize: 24,
    padding: 10,
    borderRadius: 5,
    borderWidth: 2,       
  },
  arrowRow: { 
    paddingTop: height / 4,  
    marginBottom: 100,        
    flexDirection: 'row',
    justifyContent: 'space-between',   
    width: (width/10)*8,  
  },
  deckViewContainer : { 
    flex:1,   
    alignSelf: 'center', 
    alignItems: 'center',       
    justifyContent: 'center',        
    width: (width/10)*8,      
  },    
  header: {
    alignSelf: 'center', 
    marginTop: 50, 
  }, 
  headerText: {
    fontWeight: '600',
    fontSize: 20,    
  },
  titleStyle: {   
    fontSize: 20,
    fontWeight: '600',
    alignSelf: 'center', 
    color: '#000000',
    borderWidth: 1,
    borderColor: '#262626',
    borderRadius: 15,
    padding: 40,    
    backgroundColor: '#999999',
    width: (width/10)*5,            
  },
});


class DeckScreen extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = store.getState();
    this.init(props);

  }
  init = (props) => {
    this.updateManager = props.updateManager;
    this.manifest = this.updateManager.getManifest();
    this.showDeckSwitcher = props.showDeckSwitcher;
    this.hideDeckSwitcher = props.hideDeckSwitcher;
 
    console.log('Manifest in deck screen:', this.updateManager.getManifest());   

    this.deckViews = [];
    if (this.manifest !== undefined) {
      this.manifest.forEach(entry => {

        this.deckViews.push(
        <View style={styles.arrowRow} >
          <Icon type="entypo" name="arrow-bold-left" iconStyle={styles.icon}
            onPress={() => { this.prevDeck(); }} size={50} />
          
          <Menu renderer={SlideInMenu} >    
              <MenuTrigger>
                <Text style={styles.titleStyle} numberOfLines={1}>{entry.value}</Text>
              </MenuTrigger>   
              <MenuOptions>
                <MenuOption onSelect={() => { this.showNewDecknameDialog(this.onNewDeck); }}>   
                  <Icon type="Material-Icons" name="note-add" iconStyle={styles.icon}
                     size={50} />
                </MenuOption>
                <MenuOption onSelect={() => { this.showDeleteDeckDialog(); }} >
                  <Icon type="Material-Community-Icons" name="delete-forever" iconStyle={styles.icon}
                     size={50} />   
                </MenuOption>
                <MenuOption onSelect={() => { this.showNewDecknameDialog(this.editDeck); }}> 
                  <Icon type="font-awesome" name="edit" iconStyle={styles.icon}
                     size={50} />  
                </MenuOption> 
              </MenuOptions>
            </Menu>
          <Icon type="entypo" name="arrow-bold-right" iconStyle={styles.icon}
            onPress={() => { this.nextDeck(); }} size={50} />
        </View>
        );
      });
    }
    else
      this.manifest = [];

    if (this.deckViews.length > 0) {
      this.currentDeckview = this.deckViews[0];
      this.selectedManifestIndex = 0; 
    }
    else
      this.currentDeckview = <View />

  }

  componentWillReceiveProps = (nextProps) => {
    this.init(nextProps);
  }
  prevDeck = (state) => {
    if (this.deckViews.length <= 1 || !this.showDeckSwitcher)
      return;
    if (this.selectedManifestIndex == 0) {
      this.currentDeckview = this.deckViews[this.deckViews.length - 1];
      this.selectedManifestIndex = this.deckViews.length - 1;
    }
    else {
      this.selectedManifestIndex--;
      this.currentDeckview = this.deckViews[this.selectedManifestIndex];
    }
    this.setState({});
  }
  nextDeck = (state) => {
    if (this.deckViews.length <= 1 || !this.showDeckSwitcher)
      return;
    if (this.selectedManifestIndex == this.deckViews.length - 1) {
      this.currentDeckview = this.deckViews[0];
      this.selectedManifestIndex = 0;
    }
    else {
      this.selectedManifestIndex++;
      this.currentDeckview = this.deckViews[this.selectedManifestIndex];
    }
    this.setState({});
  }

  showNewDecknameDialog = (callback) => {
    this.p.prompt({
      title: 'Name!',
      subtitle: 'Enter new deckname:',
      onSubmit: value => callback(value),

      // typical TextInput props ..
      autoFocus: true,
      clearButtonMode: 'always',
      placeholder: 'Type here ..',
      underlineColorAndroid: 'transparent',
      defaultValue: ''
    });
  }
  showDeleteDeckDialog = () => {
    this.p.confirm({
      title: 'Delete',
      subtitle: 'Are you sure you want to delete: ' + this.manifest[this.selectedManifestIndex].value + '?',
      onConfirm: yes => { if (yes) this.onDeleteDeck() },
    });
  }
  onNewDeck = (deckName) => {
    if (deckName === undefined || deckName.length === 0) {
      Alert.alert('Error', 'Invalid name!', [
        { text: 'Ok', onPress: () => { } }
      ]);
    }
    let updatedManifest = this.updateManager.addDeck(deckName);
    if (updatedManifest !== null) {
      this.init(this.props);
      this.setState({});
    }
    else {
      Alert.alert('Error', 'Name is taken!', [
        { text: 'Ok', onPress: () => { } }
      ]);
    }
  }
  onDeleteDeck = () => { 
    console.log('Going to delete:', this.manifest[this.selectedManifestIndex]);  
    if (this.manifest.length > 1) { 
      this.manifest = this.updateManager.deleteDeck(this.manifest[this.selectedManifestIndex]);
      this.updateManager.selectDeck(this.manifest[0]);  
      this.init(this.props);
      Alert.alert('Deleted', 'Deck is deleted!', [
        { text: 'Ok', onPress: () => { } }
      ]);
      this.setState({});
    }
    else {
      Alert.alert('Error', 'There must be at least one deck!', [
        { text: 'Ok', onPress: () => { } }
      ])
    }
  }
  editDeck = (deckName) => {
    if (deckName === undefined || deckName.length === 0) {
      Alert.alert('Error', 'Invalid name!', [
        { text: 'Ok', onPress: () => { } }
      ]);
    }
    let editSuccessful = this.updateManager.editDeck(deckName, this.manifest[this.selectedManifestIndex]);   
    if (editSuccessful) { 
      this.init(this.props);
      this.setState({});  
    }
    else {
      Alert.alert('Error', 'Name is taken!', [
        { text: 'Ok', onPress: () => { } }
      ]);
    }
  }
  onCancel = () => {
    this.showDeckSwitcher = false;
    this.hideDeckSwitcher();
    this.setState({});
  }
  onSubmit = () => {
    console.log('Submitting index:', this.selectedManifestIndex); 
    console.log('Submitting entry:', this.manifest[this.selectedManifestIndex]);   
    if (this.selectedManifestIndex > -1) {
      this.updateManager.selectDeck(this.manifest[this.selectedManifestIndex]);
      this.showDeckSwitcher = false;
      this.hideDeckSwitcher();   
      this.setState({});
    }
  }

  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 30,
    };

    return (
      <GestureRecognizer
        onSwipeLeft={(state) => this.nextDeck(state)}
        onSwipeRight={(state) => this.prevDeck(state)}
        config={config}
      >
        <Modal
          visible={this.showDeckSwitcher}
          animationType='slide'
          presentationStyle='overFullScreen'>
          <MenuProvider>
            <View style={styles.container}>
              <View style={styles.header}>   
              <Text style={styles.headerText}>Deck: {this.selectedManifestIndex + 1}/{this.manifest.length}</Text>
              </View>
              <View style={styles.deckViewContainer}>
              {this.currentDeckview}
              </View>
              <TouchableHighlight style={[styles.button]} onPress={() => this.onSubmit()}>
                <Text style={styles.buttonText}>Select</Text>
              </TouchableHighlight>
              <TouchableHighlight style={[styles.button, styles.cancelButton]} onPress={() => this.onCancel()}>
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableHighlight> 
            </View>
          </MenuProvider>
          <Prompt ref={ref => (this.p = ref)} />     
        </Modal> 
      </GestureRecognizer>

    );
  }
}

DeckScreen.propTypes = {
  updateManager: PropTypes.object.required,
  showDeckSwitcher: PropTypes.bool.required,
  hideDeckSwitcher: PropTypes.func.required,
}
export default DeckScreen; 