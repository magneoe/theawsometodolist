
import React from 'react';
import { View, Text, StyleSheet, TouchableHighlight, SectionList, Alert, RefreshControl } from 'react-native';
import { PropTypes } from 'prop-types';
import Taskrow from './Taskrow';
import store from './../../store/TodoStore';
import { LIST_TYPE_TODO_ID, LIST_TYPE_REMINDER_ID, WEEK_OVERVIEW_ID } from './../../store/global_constants';
import { Icon } from 'react-native-elements';

const styles = StyleSheet.create({
  container: {
    paddingTop: 0,
    backgroundColor: 'transparent',
    flex: 1,
    justifyContent: 'flex-start',
  },
  button: {
    height: 45, 
    borderColor: '#05A5D1', 
    borderWidth: 2,
    backgroundColor: '#333',
    margin: 15,  
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6 
  },
  buttonText: {
    color: '#FAFAFA',
    fontSize: 20,
    fontWeight: '600'
  },
  sectionHeader: {
    backgroundColor: 'rgba(191, 191, 191, 0.4)',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 15,
    borderWidth: 1,
    borderBottomWidth: 3,
    borderColor: 'rgba(191, 191, 191, 1.0)',
    borderRadius: 3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  sectionHeaderTitle: {
    marginLeft: 15,
    marginBottom: 5,
    fontWeight: '500',
    fontSize: 14,
  },
  columnListType: {
    flex: 2, 
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  columnIconRight: {
    flex: 2,   
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleContainer: {
    flex: 4,   
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
});

class Tasklist extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = store.getState();
    store.subscribe(() => {
      this.setState(store.getState());
    });
    this.init(props);
  }

  init = (props) => {
    this.categories = props.categories;
    if (this.categories === undefined || this.categories === null)
      this.categories = [];

    this.sections = this.categories.map(cat => {
      if (cat.id === WEEK_OVERVIEW_ID)
        return { data: cat.todos, title: cat.name, id: cat.id, cat }
      else
        return { data: cat.overview.todos, title: cat.name, id: cat.id, cat }
    });
  }
  componentWillReceiveProps(nextProps) {
    this.init(nextProps);
  }

  deleteCategory = (category, goBack) => {
    Alert.alert(
      'Delete category', 'Delete category: "' + category.name + '"?',
      [
        { text: 'Ok', onPress: () => this.props.onDeleteCategory([category], goBack) },
        { text: 'Cancel', onPress: () => { } },
      ]
    );
  }
  keyExtractor = (item, index) => { return item.id }

  renderRow = (item) => {
    return (
      <Taskrow listItem={item.item}
        onRemoveTask={this.props.onRemoveTask}
        onCheckTask={this.props.onCheckTask}
        editTask={this.props.editTask}
        category={item.section.cat}
        showHeaderBackButton={true} />  
    );
  }
  renderSectionHeader = ({ section }) => {
    let listType = section.cat.listType;
    let listTypeIcon = <View />;
    if (listType === LIST_TYPE_TODO_ID)
      listTypeIcon = <Icon type="MaterialIcons" name="low-priority" size={20} />
    else if (listType == LIST_TYPE_REMINDER_ID)
      listTypeIcon = <Icon type="evilicon" name="clock" size={20} />

    return (
      <View style={styles.sectionHeader}> 
        <View style={[styles.column, styles.titleContainer]}>
          <TouchableHighlight underlayColor={'rgba(0,0,0,0.1)'}
            delayLongPress={1000}
            onLongPress={() => this.deleteCategory(section.cat, false)}
            onPress={() => { this.props.expandCategory(section.cat) }} >
            <Text style={styles.sectionHeaderTitle}>
              {section.title} ({section.data.length}/{this.props.getTotalCategoryLength(section.cat)})</Text>
          </TouchableHighlight>
        </View>
        <View style={styles.columnListType}>
          {listTypeIcon}
        </View>      
        <View style={styles.columnIconRight}>
          <Icon type="MaterialIcons" name="keyboard-arrow-right"
            onPress={() => { this.props.expandCategory(section.cat) }} />
        </View>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <SectionList refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.props.updateRequested} />}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderRow}
          extraData={this.state}
          renderSectionHeader={this.renderSectionHeader}
          sections={this.sections} />
        <TouchableHighlight style={styles.button} onPress={() => { this.props.onAddNewItem(null) }} >
          <Text style={styles.buttonText}>Add</Text>
        </TouchableHighlight>
      </View>
    );
  }
}

Tasklist.propTypes = {
  onCheckTask: PropTypes.func.required,
  onRemoveTask: PropTypes.func.required,
  onAddNewItem: PropTypes.func.required,
  editTask: PropTypes.func.required,
  onDeleteCategory: PropTypes.func.required,
  categories: PropTypes.arrayOf(PropTypes.object).required,
  expandCategory: PropTypes.func.required,
  getTotalCategoryLength: PropTypes.func.required,
  updateRequested: PropTypes.func.isRequired,
};

export default Tasklist;
