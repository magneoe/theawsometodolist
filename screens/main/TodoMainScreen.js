import React from 'react';
import { Text, StyleSheet, View, Image, Dimensions, AppState, SafeAreaView, TouchableHighlight } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { Icon, Header } from 'react-native-elements';

import Tasklist from './Tasklist';
import TaskformScreen from './../forms/TaskformScreen';
import MainTabNavigator from './../../navigation/MainTabNavigator';
import CategoryformScreen from './../forms/CategoryformScreen';
import TasklistExpanded from './../TasklistExpanded';
import store from './../../store/TodoStore';
import FileHandler from './../../store/FileHandler';
import CrudTaskManager from './../../store/crudTaskManager';
import CrudCategoryManager from './../../store/crudCategoryManager';
import UpdateManager from './../../store/updateManager';
import DeckScreen from './DeckScreen';
import Expo from 'expo';

import {
  INIT_STATE, ROUTE_ADD_CATEGORY,
  ROUTE_MAIN_TAB_NAVIGATOR,
  DISPAY_CATEGORIES, ROUTE_ADD_TASK, ROUTE_TASK_FORM,
  WEEK_OVERVIEW_ID, WEEK_ID_DAY_1, WEEK_ID_DAY_7,
  STORE_NAME, STATE_KEY, MANIFEST_KEY,
} from './../../store/global_constants';


const styles = StyleSheet.create({
  container: {
    paddingTop: 5,
    backgroundColor: '#F7F7F7',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  headerImage: {
    height: 50,
    resizeMode: 'contain',
  },
  headerStyle: {
    borderBottomWidth: 2,
    borderBottomColor: '#d9d9d9',
    height: 60,
  },
  deckSwitcherContainer: {
    flex:1,
    flexDirection: 'column',
    alignItems: 'center',    
    width: 75,   
    justifyContent: 'space-around',   
    right: 45,               
  },
});

class TodoMainScreen extends React.Component {

  constructor(props, context) {
    super(props, context);

    //Initial values - default values  
    
    this.state = store.getState();
    store.subscribe(() => {
      this.setState(store.getState());
    });
    this.showDeckSwitcher = false;    
    if (!this.state.initialRead) {  
      store.dispatch({ type: INIT_STATE });           
      this.fileHandler = new FileHandler();
      this.updateManager = new UpdateManager(this.fileHandler, this.initData, this.handleError);   

      this.updateManager.startRefreshing(); 
      this.fileHandler.readFromLocalStorage(STORE_NAME, MANIFEST_KEY,
      this.updateManager.loadManifest, this.handleError);  
      
    } 
    this.crudTaskManager = new CrudTaskManager(this.fileHandler, this.handleError, this.updateManager.getActiveManifestEntry);
    this.crudCategoryManager = new CrudCategoryManager(this.fileHandler, this.handleError, this.updateManager.getActiveManifestEntry);    
  }
  initData = (initCategories) => {
    console.log('Has been read:', initCategories);
    if (!this.state.refreshing)
      this.updateManager.startRefreshing(); 
    store.dispatch({
      type: INIT_STATE,
      initCategories,
    });
    this.updateRequested();
    this.testGoogleLogin();
  }
  testGoogleLogin = () => {
    let accessToken;
    try {
        Expo.Google.logInAsync({
            androidClientId: '570751705505-ggnptf29n7flvugtmfe4d9g3j50geq66.apps.googleusercontent.com',
            iosClientId: '570751705505-jrrt5tllnhtl4p4kgk97sigf61q1k1du.apps.googleusercontent.com', 
            scopes: ['profile', 'https://www.googleapis.com/auth/calendar.readonly'],        
        }).then((res) => {console.log('Result', res)         
        if(res.type === 'success'){ 
          accessToken = res.accessToken;     
          let query = 'https://www.googleapis.com/calendar/v3/calendars/magneoe@gmail.com/events?orderBy=startTime&singleEvents=true&timeMin=' + new Date().toISOString();
          console.log('Query:', query);  
          fetch(query, {   
            headers: { Authorization: `Bearer ${accessToken}`}, 
          }).then(result => console.log('UserInfoResonse', result));     
          
        }
      }); 
    }
    catch(error){console.log(error); }    
  }
  handleError = (error) => {
    console.log('Handle error:', error);
    this.updateManager.stopRefresh();
  }
  updateRequested = () => {
    console.log('Updating the app!');
    this.updateManager.updateWeekviewAll();
    //Update google cal sync here 
    this.updateManager.stopRefresh();
  }
  hideDeckSwitcher = () => {
    this.showDeckSwitcher = false;
  }
  navigate = (routeDestination, params) => {
    const { navigate } = this.props.navigation;
    navigate(routeDestination, params);
  }
  onAddNewItem = (selectedCategories) => {
    this.crudTaskManager.addNewItem(selectedCategories, this.navigate, this.onAddTask,
      this.addCategory, this.expandCategory);
  }
  onAddTask = (newTask, newCategory, formerCategory) => {
    this.crudTaskManager.onAddTask(newTask, newCategory, formerCategory);
    this.updateManager.updateWeekviewAll();
  }
  onRemoveTask = (task, category) => {
    this.crudTaskManager.onRemoveTask(task, category);
    this.updateManager.updateWeekviewAll();
  }
  onRemoveAllTasks = (category) => {
    this.crudTaskManager.onRemoveAllTasks(category);
    this.updateManager.updateWeekviewAll();
  }
  onCheckTask = (tasks, category, isChecked) => {
    this.crudTaskManager.onCheckTask(tasks, category, isChecked);
    this.updateManager.updateWeekviewAll();
  }
  editTask = (task, category, showHeaderBackButton) => {
    this.crudTaskManager.editTask(task, category, showHeaderBackButton, this.navigate,
      this.onAddTask, this.addCategory);
    this.updateManager.updateWeekviewAll(); 
  }
  addCategory = (name, selectedListType) => {
    return this.crudCategoryManager.addCategory(name, selectedListType);
  }
  deleteCategory = (categoriesToRemove, goBack) => { 
    this.crudCategoryManager.deleteCategory(categoriesToRemove);
    this.updateManager.updateWeekviewAll();
    if (goBack)
      this.props.navigation.goBack(null);  
  } 
   
  expandCategory = (category) => {
    this.crudCategoryManager.expandCategory(category, this.navigate,
      this.onCheckTask, this.onRemoveTask,
      this.onAddNewItem, this.editTask,
      this.onRemoveAllTasks, this.deleteCategory);
  }
  getTotalCategoryLength = (category) => {
    return this.crudCategoryManager.getTotalLength(category);
  }
  switchDeck = () => { 
    this.showDeckSwitcher = true;
    this.setState({});
  }
  hideDeckSwitcher = () => {
    this.showDeckSwitcher = false;
  } 
  
  render() {
    return (
      <View style={{ flex: 1 }}>
        <View style={styles.headerStyle}>
          <Header
            leftComponent={{icon:'menu'}}  
            centerComponent={<Image style={styles.headerImage} source={require('./../../assets/frontpage_image.png')} />}
            rightComponent={ 
              <View style={styles.deckSwitcherContainer}>
              <Text numberOfLines={1}>{this.updateManager.getActiveManifestEntry().value}</Text>
              <Icon type="font-awesome"
                name="exchange" size={22} onPress={()=>{this.switchDeck()}} /> 
                </View>
                }     
            backgroundColor="#F7F7F7" 
          />
        </View>
        <DeckScreen showDeckSwitcher={this.showDeckSwitcher} hideDeckSwitcher={this.hideDeckSwitcher} updateManager={this.updateManager} />  
        <View style={styles.container}>
          <Tasklist
            onAddNewItem={this.onAddNewItem}
            onRemoveTask={this.onRemoveTask}
            onCheckTask={this.onCheckTask}
            editTask={this.editTask}
            onDeleteCategory={this.deleteCategory}
            expandCategory={this.expandCategory}
            categories={[this.state.weekview.overview].concat(this.state.categories)}
            getTotalCategoryLength={this.getTotalCategoryLength}
            updateRequested={this.updateRequested}
          />
        </View>
      </View>
    );
  }
}

export default TodoMainScreen;


