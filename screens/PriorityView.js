import React from 'react';
import { Text, View, Picker, StyleSheet } from 'react-native';
import { PropTypes } from 'prop-types'; 

const styles = StyleSheet.create({
  label: {
    marginLeft: 10,
    fontSize: 15,
    fontWeight: '500',
  },
  input: {
    borderWidth: 1,
    borderColor: '#D7D7D7',
    marginLeft: 10,
    marginRight: 10,
    padding: 15,
    borderRadius: 3,
  },
});

export const PriorityView = (props) => {
    
}
