import store from './TodoStore';
import {
    UPDATE_WEEK_VIEW, WEEK_ID_DAY_1,
    WEEK_ID_DAY_2, WEEK_ID_DAY_3,
    WEEK_ID_DAY_4, WEEK_ID_DAY_5,
    WEEK_ID_DAY_6, WEEK_ID_DAY_7,
    DISPAY_CATEGORIES, STOP_REFRESHING, START_REFRESHING, STORE_NAME, MANIFEST_KEY, STATE_KEY,
} from './global_constants'; 

class UpdateManager {
    constructor(filehandler, initData, handleError) {
        this.filehandler = filehandler;
        this.initData = initData;
        this.handleError = handleError;
        this.state = store.getState();
        store.subscribe(() => {
            this.state = store.getState();
        });
        this.defaultWeekview = [WEEK_ID_DAY_1, WEEK_ID_DAY_2, WEEK_ID_DAY_3, WEEK_ID_DAY_4, WEEK_ID_DAY_5, WEEK_ID_DAY_6, WEEK_ID_DAY_7];
    }
    stopRefresh = () => {
        store.dispatch({
            type: STOP_REFRESHING,
        });
    }
    startRefreshing = () => {
        store.dispatch({
            type: START_REFRESHING,
        });
    }
    /*
    * MANIFEST LOADING METHODS 
    */
    loadManifest = (manifest) => {
        if (manifest === null || manifest === undefined) {
            let defaultManifest = [
                { key: STATE_KEY, value: 'My deck' },
            ];
            this.filehandler.writeToLocalStorage(STORE_NAME, MANIFEST_KEY, defaultManifest, this.handleError);
            this.manifest = defaultManifest;
        }
        else
            this.manifest = manifest;   
        //this.selectedDeckKey = this.manifest[0].key;
        this.loadDeck(this.manifest[0].key);
    }
    loadDeck = (key) => {
        console.log('Loading deck:', key);
        this.filehandler.readFromLocalStorage(STORE_NAME, key, this.initData, this.handleError);
    }
    addDeck = (name) => {
        let addedSuccessfully = false;
        if (this.manifest.find(entry => { return entry.value === name }) === undefined) {
            this.manifest = this.manifest.concat([{ key: name + '_entry', value: name }]);    
            console.log('New manifest:', this.manifest);
            this.filehandler.writeToLocalStorage(STORE_NAME, MANIFEST_KEY, this.manifest, this.handleError);
            addedSuccessfully = true;
        }
        return addedSuccessfully; 
    }
    //Manifest must be updated before the aync calls to the filehandler - to avoid dirty reads 
    selectDeck = (entry) => {
        //this.selectedDeckKey = entry.key;
        this.manifest = [entry].concat(this.manifest.filter(curr => { return curr.key !== entry.key }));
        this.filehandler.writeToLocalStorage(STORE_NAME, MANIFEST_KEY, this.manifest, this.handleError);
        this.loadDeck(this.getActiveManifestEntry().key);  //this.selectedDeckKey
    }
    //Manifest must be updated before the aync calls to the filehandler - to avoid dirty reads
    deleteDeck = (entry) => {
        this.manifest = this.manifest.filter(curr => { return curr.key !== entry.key });
        this.filehandler.deleteEntry(STORE_NAME, entry.key, this.handleError);   
        this.filehandler.writeToLocalStorage(STORE_NAME, MANIFEST_KEY, this.manifest, this.handleError);
        return this.manifest; 
    }
    editDeck = (name, entry) => {
        let editSuccessful = false;
        if (this.manifest.find(entry => { return entry.value === name }) === undefined) {
            entry.value = name;
            let givenIndex = this.manifest.findIndex(curr => { return curr.key === entry.key });
            this.manifest[givenIndex] = entry;
            this.manifest = [entry].concat(this.manifest.filter(curr => { return curr.key !== entry.key }));
            this.filehandler.writeToLocalStorage(STORE_NAME, MANIFEST_KEY, this.manifest, this.handleError);
            editSuccessful = true;
        }
        return editSuccessful;
    }
    getManifest = () => {
        return this.manifest;
    }
    getActiveManifestEntry = () => {
        if(this.manifest === null || this.manifest === undefined)
            return [{key:'', value:''}];
        return this.manifest[0];
    }
    /*
    * WEEK UPDATE METHODS
    */
    updateWeekviewAll = () => {
        this.updateWeekview(this.defaultWeekview);
    }
    updateWeekview = (weekviewCategoryIds) => {
        if (weekviewCategoryIds !== undefined && weekviewCategoryIds !== null) {
            let currDate = new Date();
            let weekCategoriesToUpdate = [];

            for (let index = 0; index < this.state.weekview.categories.length; index++) {
                if (weekviewCategoryIds.find(id => { return id === this.state.weekview.categories[index].id }) !== undefined) {

                    let dateInWeek = this.getDatePlusGivenDays(currDate, index);
                    let assignedTasks = this.getTasksAssignedToDate(dateInWeek, (index === 0 ? true : false)); //First day of week will accumulate all previous tasks
                    let name = this.getCurrentDateFormatted(dateInWeek);

                    this.state.weekview.categories[index].name = name;
                    this.state.weekview.categories[index].todos = assignedTasks;
                }
                weekCategoriesToUpdate.push(this.state.weekview.categories[index]);
            }
            store.dispatch({
                type: UPDATE_WEEK_VIEW,
                weekCategoriesToUpdate,
                weekOverviewTasks: this.getUpdatedWeekOverview(weekCategoriesToUpdate),
            });
        }
    }

    getUpdatedWeekOverview = (weekCategoriesToUpdate) => {
        let overviewTasks = [];
        weekCategoriesToUpdate.forEach(weekDayCat => {
            if (overviewTasks.length < DISPAY_CATEGORIES) {
                overviewTasks = overviewTasks.concat(weekDayCat.todos.slice(0, DISPAY_CATEGORIES - overviewTasks.length));
            }
        });
        return overviewTasks;
    }

    getDatePlusGivenDays = (currDate, numberOfDays) => {
        let currTime = currDate.getTime();
        let milisToAdd = 1000 * 60 * 60 * 24 * numberOfDays;
        return new Date(currTime + milisToAdd);
    }

    getTasksAssignedToDate = (date, first) => {
        let assignedTasks = [];
        let day = date.getDate();
        let month = date.getMonth();
        let year = date.getYear();

        if (this.state.categories === undefined || this.state.categories === null)
            return assignedTasks;

        this.state.categories.forEach(cat => {
            if (first) {
                cat.todos.forEach(todo => {
                    if (todo.isActive &&
                        todo.timeEnabled &&
                        todo.date.getDate() <= day &&
                        todo.date.getMonth() <= month &&
                        todo.date.getYear() <= year) {
                        assignedTasks.push(todo);
                    }
                });
            }
            else {
                cat.todos.forEach(todo => {
                    if (todo.isActive &&
                        todo.timeEnabled &&
                        todo.date.getDate() === day &&
                        todo.date.getMonth() === month &&
                        todo.date.getYear() === year) {
                        assignedTasks.push(todo);
                    }
                });
            }
        });
        return assignedTasks;
    }
    getCurrentDateFormatted = (date) => {
        let label = '';
        try {
            let utcString = date.toUTCString().split(' ');
            label += utcString[0] + ' ';
            label += utcString[1] + ' ';
            label += utcString[2] + ' ';
        } catch (error) { }
        return label;
    }
}

export default UpdateManager;


/*
    // sleep time expects milliseconds
    sleep = (time) => {
        return new Promise((resolve) => setTimeout(resolve, time));
    }
     /*
    startUpdateCycle = (sleepCycle, setUpdateLabelText) => {
        this.state.lastUpdate = new Date();   
        setUpdateLabelText('Last full update: < 15 min ago');
        this.sleep(sleepCycle).then(() => { 
            let label = 'Last full update: ';
            let timeSinceLastUpdate = (new Date().getTime() - this.state.lastUpdate.getTime());      

                if(timeSinceLastUpdate < (1000 * 60 )) 
                    label += '< 30 min ago';
                else if(timeSinceLastUpdate < (1000 * 60 * 60))
                    label += '< 1h ago';
                else if(timeSinceLastUpdate < (1000 * 60 * 60 * 5))
                    label += '< 5h ago';
                else
                    label += '> 24h ago';  
            setUpdateLabelText(label);
        });
        
    }*/