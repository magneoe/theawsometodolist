

export const DISPAY_CATEGORIES = 3;

//Priority
export const PRI_LOW_ID = 1;
export const PRI_NORMAL_ID = 2;
export const PRI_HIGH_ID = 3;
export const PRIORITIES = [
  { name: '!', id: PRI_LOW_ID },
  { name: '!!', id: PRI_NORMAL_ID },
  { name: '!!!', id: PRI_HIGH_ID }  
];

//List types 
export const LIST_TYPE_TODO_ID = 1;
export const LIST_TYPE_REMINDER_ID = 2;
export const LIST_TYPE_WEEKVIEW_ID = 3;   
export const LIST_TYPES = [ //Selectable list types 
  { id: LIST_TYPE_TODO_ID, name: 'Todos' },
  { id: LIST_TYPE_REMINDER_ID, name: 'Reminders' } 
];

//Constants for todo store
export const INIT_STATE = "INIT_STATE";
export const ADD_CATEGORY = "ADD_CATEGORY";
export const REMOVE_CATEGORIES = "REMOVE_CATEGORIES";
export const UPDATE_CATEGORIES = "UPDATE_CATEGORIES";
export const UPDATE_WEEK_VIEW = "UPDATE_WEEK_VIEW";
export const STOP_REFRESHING = "STOP_REFRESHING"; 
export const START_REFRESHING = "START_REFRESHING"; 

//Route constants
export const ROUTE_TODO_MAIN = 'todoMain';
export const ROUTE_MAIN_TAB_NAVIGATOR = 'mainTabNavigator';
export const ROUTE_ADD_CATEGORY = 'addCategory';
export const ROUTE_TASKLIST_EXPANDED = 'tasklistExpanded';
export const ROUTE_ADD_TASK = 'addTask';
export const ROUTE_TASK_FORM = 'taskForm';

export const INITITAL_ROOT_ROUTE = 'todoMain';


export const STORE_NAME = 'TodoStore';
export const STATE_KEY = 'localState';
export const MANIFEST_KEY = 'manifest'; 


//Weekview constants 
export const WEEK_OVERVIEW_ID = 1;
export const WEEK_ID_DAY_1 = 2;
export const WEEK_ID_DAY_2 = 3;
export const WEEK_ID_DAY_3 = 4;
export const WEEK_ID_DAY_4 = 5;
export const WEEK_ID_DAY_5 = 6;
export const WEEK_ID_DAY_6 = 7;
export const WEEK_ID_DAY_7 = 8;
