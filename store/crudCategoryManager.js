import store from './TodoStore';
import {
  ADD_CATEGORY, REMOVE_CATEGORIES, UPDATE_WEEK_VIEW,
  STORE_NAME, WEEK_OVERVIEW_ID,
  ROUTE_TASKLIST_EXPANDED
} from './global_constants';

class CrudCategoryManager {
  constructor(fileHandler, handleError, getActiveManifestEntry) {
    this.fileHandler = fileHandler;
    this.handleError = handleError;
    this.getActiveManifestEntry = getActiveManifestEntry;

    this.state = store.getState();
    store.subscribe(() => {
      this.state = store.getState();
    });
  }
  dispatchEvent = (action) => {
    store.dispatch(action);
    this.fileHandler.writeToLocalStorage(STORE_NAME, this.getActiveManifestEntry(),
      store.getState().categories, this.handleError);
  }
  addCategory = (name, selectedListType) => { 
    try {
      if (this.state.categories.findIndex(e => { return e.name === name }) !== -1)
        return null; //Invalid
      let newCategory = {
        overview: { todos: [] },
        id: this.getNextCategoryId(this.state.categories),
        name: name,
        readOnly: false,
        listType: selectedListType,
        todos: [],
      }
      this.dispatchEvent({
        type: ADD_CATEGORY,
        newCategory,
      });
      //Is valid
      return newCategory;
    } catch (error) {
      this.handleError(error);
    }
  }

  deleteCategory = (categoriesToRemove) => {
    try {
      this.dispatchEvent({
        type: REMOVE_CATEGORIES,
        categoriesToRemove,
      });
    }
    catch (error) {
      this.handleError(error);
    }
  }

  getTotalLength = (category) => {
    try {
      let totalLength = 0;
      if (category.todos === undefined)
        return totalLength;

      switch (category.id) {
        case WEEK_OVERVIEW_ID:
          if (this.state.weekview !== undefined) {
            this.state.weekview.categories.forEach(cat => {
              totalLength += cat.todos.length;
            });
          }
          break;
        default:
          totalLength = category.todos.length;
      }
      return totalLength;
    }
    catch (error) {
      this.handleError(error);
    }
  }

  expandCategory = (category, navigate, onCheckTask, onRemoveTask,
    onAddNewItem, editTask, onRemoveAllTasks, deleteCategory) => {
    try {
      let selectedCategories = [];
      let headerTitle = '';
      if (category.id === WEEK_OVERVIEW_ID) {
        selectedCategories = selectedCategories.concat(this.state.weekview.categories);
        headerTitle = 'Week overview';
      }
      else {
        selectedCategories.push(category);
        headerTitle = selectedCategories[0].name;
      }
      //Setting routing parameters
      let params = {
        onCheckTask: onCheckTask,
        onRemoveTask: onRemoveTask,
        onAddNewItem: onAddNewItem,
        editTask: editTask,
        onDeleteCategory: deleteCategory,
        onRemoveAllTasks: onRemoveAllTasks,
        getTotalCategoryLength: this.getTotalLength,
        headerTitle,
        selectedCategories,
      };
      navigate(ROUTE_TASKLIST_EXPANDED, params);
    }
    catch (error) {
      this.handleError(error);
    }
  }

  /*
  * HELPER METHODS
  */
  getNextCategoryId = (categories) => {
    let highestId = -1;
    //Lowest id
    if (categories.length === 0)
      return 10;
    categories.forEach(cat => {
      if (cat.id > highestId)
        highestId = cat.id;
    });
    return highestId + 1;
  }
}

export default CrudCategoryManager;