
import {AsyncStorage} from 'react-native';

class FileHandler {
  constructor(){}
  
  readFromLocalStorage = (storeName, key, resultCallback, errorCallback) => {
      console.log('Reading data...'); 
        AsyncStorage.getItem('@' + storeName + ':' + key). 
          then((result) => resultCallback(JSON.parse(result))).
          catch(error => errorCallback(error)); 
  }
 
  writeToLocalStorage = (storeName, key, data, errorCallback) => { 
    console.log('Writing data...');   
    AsyncStorage.setItem('@' + storeName + ':' + key, JSON.stringify(data)).catch(error => errorCallback(error));
  }

  deleteEntry = (storeName, key, errorCallback) => {
    console.log('Deleting key ' + key);
    AsyncStorage.removeItem('@' + storeName + ':' + key).catch(error => errorCallback(error));
  }
}
export default FileHandler; 
