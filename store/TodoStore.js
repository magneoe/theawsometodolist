import React from 'react';
import { createStore } from 'redux';
import {
  INIT_STATE, ADD_CATEGORY, UPDATE_CATEGORIES, UPDATE_WEEK_VIEW, REMOVE_CATEGORIES, WEEK_OVERVIEW_ID,
  WEEK_ID_DAY_1, WEEK_ID_DAY_2, WEEK_ID_DAY_3, WEEK_ID_DAY_4, WEEK_ID_DAY_5, WEEK_ID_DAY_6, WEEK_ID_DAY_7,
  STOP_REFRESHING, START_REFRESHING,
} from './global_constants'; 

const defaultState = {
  initialRead: false,
  refreshing: false,
  lastUpdate: '', 
  google: { user: '', pw: '', lastSync: '' },
  weekview: {
    overview: {
      id: WEEK_OVERVIEW_ID,
      name: 'Week',
      listType: 3,
      readOnly: true,
      todos: []
    },
    categories: [{
      id: WEEK_ID_DAY_1,
      name: '',
      listType: 3,
      readOnly: true,
      todos: []
    },
    {
      id: WEEK_ID_DAY_2,
      name: '',
      listType: 3,
      readOnly: true,
      todos: [],
    },
    {
      id: WEEK_ID_DAY_3,
      name: '',
      listType: 3,
      readOnly: true,
      todos: [],
    },
    {
      id: WEEK_ID_DAY_4,
      name: '',
      listType: 3,
      readOnly: true,
      todos: [],
    },
    {
      id: WEEK_ID_DAY_5,
      name: '',
      listType: 3,
      readOnly: true,
      todos: [],
    },
    {
      id: WEEK_ID_DAY_6,
      name: '',
      listType: 3,
      readOnly: true,
      todos: [],
    },
    {
      id: WEEK_ID_DAY_7,
      name: '',
      listType: 3,
      readOnly: true,
      todos: [],
    }
    ],
  }, //Normal categories
  categories: [{
    overview: {
      todos: []  
    },
    id: 10,
    name: 'General',
    readOnly: false,
    listType: 1, // Priority based
    todos: [] 
  }],  
};

getDefaultCategory = () => {
  return { 
    overview: {
      todos: []  
    },
    id: 10,
    name: 'General',
    readOnly: false,
    listType: 1, // Priority based
    todos: [] 
  };
}

todoStore = (state = defaultState, action) => {
  console.log('In todo store: ' + action.type); 
  switch (action.type) {
    case INIT_STATE:      
      if (action.initCategories !== null && action.initCategories !== undefined)  
      {   
        //Parsing date from strings to objects. 
          action.initCategories.forEach(cat => {
            cat.overview.todos.forEach(task => {
              if(task.date !== undefined && task.date !== null)
                task.date = new Date(task.date);      
            }); 
            cat.todos.forEach(task => {
              if(task.date !== undefined && task.date !== null)
                task.date = new Date(task.date);      
            });
          });
          var newState = {...state};       
          newState.initialRead = true;        
          newState.categories = action.initCategories;     
          return Object.assign({}, state, newState);   
      }
      else if(action.initCategories === null) {
        let newState = {...defaultState};  
        newState.initialRead = true;
        newState.categories = [getDefaultCategory()];             
        return Object.assign({}, newState);         
      }    
      return Object.assign({}, defaultState);    
    case ADD_CATEGORY:
      var newState = { ...state };
      newState.categories = newState.categories.concat([action.newCategory]);
      return Object.assign({}, state, newState);
      break;
    case UPDATE_CATEGORIES:
      var newState = { ...state };
      action.categories.forEach(updatedCat => {
        let catToUpdate = newState.categories.find(cat => { return cat.id === updatedCat.id });
        catToUpdate = updatedCat;
      });
      return Object.assign({}, state, newState);
      break;
    case UPDATE_WEEK_VIEW:
      var newState = { ...state };
      newState.weekview.categories = action.weekCategoriesToUpdate;
      newState.weekview.overview.todos = action.weekOverviewTasks;
      return Object.assign({}, state, newState);
      break;
    case REMOVE_CATEGORIES:
      var newState = { ...state };
      action.categoriesToRemove.forEach(catToRemove => {
        newState.categories = newState.categories.filter(cat => { return cat.id !== catToRemove.id });
      });
      return Object.assign({}, state, newState);
      break;
    case START_REFRESHING:
    var newState = { ...state };
    newState.refreshing = true; 
    return Object.assign({}, state, newState);
    break;
    case STOP_REFRESHING:
      var newState = { ...state };
      newState.refreshing = false; 
      return Object.assign({}, state, newState);
      break;
    default:
      console.log('TODO store does not recoginse the action:' + action.type);
      return state;
  }
}


export default createStore(todoStore);
