import store from './TodoStore';
import {
  storeName, stateKey, ROUTE_ADD_TASK,
  ROUTE_TASK_FORM, UPDATE_CATEGORIES,
  STORE_NAME, DISPAY_CATEGORIES,
  LIST_TYPE_TODO_ID, LIST_TYPE_REMINDER_ID,
  WEEK_ID_DAY_7, WEEK_ID_DAY_1, WEEK_OVERVIEW_ID,
  ROUTE_MAIN_TAB_NAVIGATOR, ROUTE_ADD_CATEGORY,
} from './global_constants';

class CrudTaskManager {
  constructor(fileHandler, handleError, getActiveManifestEntry) {
    this.fileHandler = fileHandler;
    this.handleError = handleError;
    this.getActiveManifestEntry = getActiveManifestEntry;

    this.state = store.getState();
    store.subscribe(() => {
      this.state = store.getState();
    });
  }
  dispatchEvent = (categoriesToUpdate, action) => {
    store.dispatch(action);
    this.updateOverviews(categoriesToUpdate);
    this.fileHandler.writeToLocalStorage(STORE_NAME, this.getActiveManifestEntry(),
      store.getState().categories, this.handleError);
  }

  onAddTask = (newTask, newCategory, formerCategory) => {
    try {
      let categoriesToUpdate = [newCategory];
      //Edit task 
      if (newTask.id !== undefined && newTask.id !== null) {
        formerCategory.todos = formerCategory.todos.filter(todo => { return todo.id !== newTask.id });
        categoriesToUpdate.push(formerCategory);
        newCategory.todos.push(newTask);
      }
      //New item
      else {
        newTask.id = this.getNextTaskId(this.state.categories);
        newCategory.todos.push(newTask);
      }

      //Sorting
      categoriesToUpdate.forEach(cat => {
        if (cat.listType === LIST_TYPE_TODO_ID)
          cat.todos.sort(this.sortTodos);
        else if (cat.listType == LIST_TYPE_REMINDER_ID)
          cat.todos.sort(this.sortReminders);
      });
      this.dispatchEvent(categoriesToUpdate, {
        type: UPDATE_CATEGORIES,
        categories: categoriesToUpdate,
      });
    } catch (error) {
      this.handleError(error);
    }
  }
  onRemoveTask = (task, category) => {
    try {
      if (task !== undefined && category !== undefined) {
        category.todos = category.todos.filter(todo => { return todo.id !== task.id });
        //If we are deleting a task on the weekview, we need to find the original category 
        if (category.id <= WEEK_ID_DAY_7 && category.id >= WEEK_ID_DAY_1) {
          category = this.state.categories.find(cat => { return (cat.todos.find(todo => { return todo.id === task.id })) !== undefined });
          category.todos = category.todos.filter(todo => { return todo.id !== task.id });
        }
        this.dispatchEvent([category], {
          type: UPDATE_CATEGORIES,
          categories: [category],
        });
      }
    } catch (error) {
      this.handleError(error);
    }
  }
  onRemoveAllTasks = (selectedCategory) => {
    try {
      let categoriesToUpdate = [];
      //If we are deleting a task on the weekview, we need to find the original category 
      if (selectedCategory.id <= WEEK_ID_DAY_7 && selectedCategory.id >= WEEK_ID_DAY_1) {
        selectedCategory.todos.forEach(task => {
          let category = this.state.categories.find(cat => { return (cat.todos.find(todo => { return todo.id === task.id })) !== undefined });
          category.todos = category.todos.filter(todo => { return todo.id !== task.id });
          if (categoriesToUpdate.findIndex(cat => { return cat.id === category.id }) === -1)
            categoriesToUpdate.push(category);
        });
      }
      selectedCategory.todos = [];
      this.dispatchEvent(categoriesToUpdate, {
        type: UPDATE_CATEGORIES,
        categories: categoriesToUpdate,
      });
    } catch (error) {
      this.handleError(error);
    }
  }
  onCheckTask = (tasks, category, isChecked) => {
    try {
      let categoriesToUpdate = [];
      if (tasks !== undefined && category !== undefined) {
        category.todos.forEach(task => {
          if (tasks.find(t => { return t.id === task.id }) !== undefined)
            task.isActive = !isChecked;
        });
        if (category.id >= WEEK_OVERVIEW_ID && category.id <= WEEK_ID_DAY_7) {
          if (tasks.length === category.todos.length) //Triggers a state change in the expanded list view 
            category.todos = [];
          tasks.forEach(task => {
            let catOfGivenTask = this.state.categories.find(cat => { return (cat.todos.find(todo => { return todo.id === task.id })) !== undefined });
            if (categoriesToUpdate.findIndex(cat => { return cat.id === catOfGivenTask.id }) === -1)
              categoriesToUpdate.push(catOfGivenTask);
          });
        }
        else
          categoriesToUpdate.push(category);

        //Sorting
        categoriesToUpdate.forEach(cat => {
          if (cat.listType === LIST_TYPE_TODO_ID)
            cat.todos.sort(this.sortTodos);
          else if (cat.listType == LIST_TYPE_REMINDER_ID)
            cat.todos.sort(this.sortReminders);
        });
        this.dispatchEvent(categoriesToUpdate, {
          type: UPDATE_CATEGORIES,
          categories: categoriesToUpdate,
        });
      }
    } catch (error) {
      this.handleError(error);
    }
  }

  editTask = (task, category, showHeaderBackButton, navigate, onAddTask, addCategory) => {
    try {
      //let categoryToExpand = null;
      let selectedCategories = [];

      //If given category is located in the week overview
      if (category.id >= WEEK_OVERVIEW_ID && category.id <= WEEK_ID_DAY_7) {
        //Setting selected category - based on the listType property on the task.  
        category = this.state.categories.find(cat => { return (cat.todos.find(todo => { return todo.id === task.id })) !== undefined });
        selectedCategories = this.state.categories.filter(cat => { return (cat.listType === task.listType && cat.id !== category.id) });
      }
      //Editing a regular category 
      else {
        selectedCategories = this.state.categories.filter(cat => { return (cat.listType === category.listType && cat.id !== category.id) });
      }
      //Setting routing parameters
      let params = {
        onAddTask: onAddTask,
        toEdit: task,
        showHeaderBackButton, 
        addCategory: addCategory,
        selectedCategories: [category].concat(selectedCategories), 
      };
      navigate(ROUTE_TASK_FORM, params);
    }
    catch (error) {
      this.handleError(error);
    }
  }

  addNewItem = (selectedCategories, navigate, onAddTask, addCategory) => {  
    try {
      let showHeaderBackButton = false; //Default
      let routeDestination = ROUTE_MAIN_TAB_NAVIGATOR; //Default route

      if (selectedCategories !== undefined && selectedCategories !== null) { 
        routeDestination = ROUTE_TASK_FORM;
        showHeaderBackButton = true;

        if (selectedCategories.length > 0 && selectedCategories[0].id === WEEK_ID_DAY_1) {
          selectedCategories = this.state.categories;
        }
      }
      else {
        selectedCategories = this.state.categories;
        if (selectedCategories.length === 0)  
          routeDestination = ROUTE_ADD_CATEGORY;
      }

      let params = { 
        onAddTask: onAddTask,
        addCategory: addCategory,
        toEdit: null,
        showHeaderBackButton,
        selectedCategories,
      };
      navigate(routeDestination, params);    
    } catch (error) {
      this.handleError(error);
    }
  }

  updateOverviews = (categoriesToUpdate) => { 
    categoriesToUpdate.forEach(categoryToUpdate => {
      let numberOfTasksToShow = (categoryToUpdate.todos.length >= DISPAY_CATEGORIES ? DISPAY_CATEGORIES : categoryToUpdate.todos.length);
      if (categoryToUpdate !== undefined && categoryToUpdate !== null) {
        categoryToUpdate.overview.todos = categoryToUpdate.todos.slice(0, numberOfTasksToShow);
      }
    });
    store.dispatch({
      type: UPDATE_CATEGORIES,
      categories: categoriesToUpdate,
    });
  }

  /*
* HELPER METHODS
*/
  getNextTaskId = (categories) => {
    let allTasks = [];
    let highestId = -1;
    categories.forEach(cat => {
      allTasks = allTasks.concat(cat.todos);
    });
    allTasks.forEach(task => {
      if (task.id > highestId)
        highestId = task.id;
    });
    return highestId + 1;
  }


  //Sort after priority - and put the inActive at the buttom.
  sortTodos = (a, b) => {
    //If both elements have the same 'isActive' then sort by priority, then title
    if (b.isActive === a.isActive) {
      if (a.priority === b.priority) {
        if (b.title < a.title)
          return 1;
        else if (b.title > a.title)
          return -1;
        return 0;
      }
      return b.priority - a.priority;
    }
    else if (b.isActive) {
      return 1;
    }
    return -1;
  }
  sortReminders = (a, b) => {
    if (b.isActive === a.isActive) {
      if (a.date !== null && b.date !== null)
        return a.date.getTime() - b.date.getTime();
      else if (a.date === null)
        return 1;
      return -1;
    }
    else if (b.isActive) {
      return 1;
    }
    return -1;
  }

  sortCategories = (a, b) => {
    if (b.name < a.name)
      return 1;
    else if (b.name > a.name)
      return -1;
    return 0;
  }
}

export default CrudTaskManager;